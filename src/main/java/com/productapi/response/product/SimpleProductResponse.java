package com.productapi.response.product;

import java.util.ArrayList;
import java.util.List;

import com.productapi.model.Product;

public class SimpleProductResponse extends ProductResponse {

	SimpleProductResponse(){}
	
	SimpleProductResponse(Product product) {
		super(product);
	}
	
	public static List<SimpleProductResponse> fromListProduct(List<Product> products) {
		List<SimpleProductResponse> list = new ArrayList<>();
		for (Product product : products) {
			list.add(new SimpleProductResponse(product));
		}
		
		return list;
	}
	
	public static SimpleProductResponse fromProduct(Product product) {
		return new SimpleProductResponse(product);
	}
}

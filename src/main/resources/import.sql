--Dummy data for in-memory database at startup

INSERT INTO PRODUCT (product_id, name, description) VALUES (1, 'leite', 'lacteo');
INSERT INTO PRODUCT (product_id, name, description) VALUES (2, 'arroz', 'grao');
INSERT INTO PRODUCT (product_id, name, description) VALUES (3, 'feijao', 'grao');

INSERT INTO IMAGE (image_id, type, product_id) VALUES (1, 'image1-1', 1);
INSERT INTO IMAGE (image_id, type, product_id) VALUES (2, 'image1-2', 1);
INSERT INTO IMAGE (image_id, type, product_id) VALUES (3, 'image1-3', 1);
INSERT INTO IMAGE (image_id, type, product_id) VALUES (4, 'image2-1', 2);
INSERT INTO IMAGE (image_id, type, product_id) VALUES (5, 'image2-2', 2);
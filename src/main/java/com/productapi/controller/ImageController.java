package com.productapi.controller;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.productapi.model.Image;
import com.productapi.response.image.ImageResponse;
import com.productapi.service.ImageService;

@Component
@Path("/image")
public class ImageController {

	private static final String URL = "/app/image/";
	
	@Autowired
	private ImageService service;
	
	@GET
	@Path("byProduct/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getImageSet(@PathParam("id") Long id) {
		List<Image> list = service.getImageSetByProductId(id);
		return Response.ok(ImageResponse.getImages(list)).build();
	}
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addImage(Image image) {
		Image i = service.save(image);
		
		if(i == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		return Response.created(URI.create(URL + i.getId())).build();
		
	}
	
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateImage(Image image) {
		if(image.getId() == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		Image retrievedImage = service.findById(image.getId());
		if(retrievedImage == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		service.save(image);
		return Response.accepted().build();
	}
	
	@POST
	@Path("/delete/{id}")
	public Response deleteImage(@PathParam("id")Long id) {
		service.delete(id);
		return Response.ok().build();
	}
}

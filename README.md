### Setup and run ###

* git clone https://mkapt@bitbucket.org/mkapt/avenue-code-api.git
* go to the project's root folder and type mvn spring-boot:run to start the application.
* to make requests, use any related program you want. [Postman] (https://www.getpostman.com/apps) is a good one.
* the application is preloaded with a dummy database sample, acessible from: http://localhost:8888/h2 (user: sa, password: leave it blank)

### Request samples ###

#### Create a product

URL: localhost:8888/app/product/add

{
"name":"Budweiser",
"description":"Beverage",
"children": [
            {
                "name": "Peanut",
                "description": "Food",
                "children": [],
                "images": []
            },
            {
                "name": "Sausage",
                "description": "Food",
                "children": [],
                "images": []
            }
        ]
}

#### Update a product

URL: localhost:8888/app/product/update

"id": 4
"name":"Miller Draft",
"description":"Beverage",
"children": [
            {
                "name": "Peanut",
                "description": "Food",
                "children": [],
                "images": []
            },
            {
                "name": "Sausage",
                "description": "Food",
                "children": [],
                "images": []
            }
        ]
}

#### Delete a product

URL: localhost:8888/app/image/delete/{id}

#### Get all products excluding relationships (child products, images)

localhost:8888/app/product/getAllSimpleProducts

#### Get all products including specified relationships (child product and/or images) 

URL: localhost:8888/app/product/getAllComplexProducts

#### Get all products excluding relationships (child products, images) using specific product identity 

URL: localhost:8888/app/product/simple/{id}

#### Get all products including relationships (child products, images) using specific product identity 

URL: localhost:8888/app/product/complex/{id}

#### Get set of child products for specific product

URL: localhost:8888/app/product/getChildren/{id}

#### Get set of images for specific product

URL: localhost:8888/app/image/byProduct/{id}

#### Create an image

URL: localhost:8888/app/image/add

{
	"type": "Food",
	"product": {
        "id": 1
    }
}

#### Update an image

URL: localhost:8888/app/image/update

{
	"id": 3
	"type": "Food",
	"product": {
        "id": 8
    }
}

#### Delete an image

URL: localhost:8888/app/image/delete/1


### Points of attention ###

#### JAX-RS
That has been the first time I have had the opportunity to develop a project using JAX-RS (I'm used to Spring Boot). I certainly
needed some googling to know how to use this API and build this project, although it's quite similar to Spring. IT is about learning
new things everyday and I have no problem at all in admiting that I needed help here and there. I'd rather be honest than fake.

#### Struggle with lazy exception and cyclic references
I have spent some time figuring out how to deal with lazy loading exceptions when fetching a product from database as well as
with cyclic reference  when marshalling objects using Jackson, two common issues when collections and bidirectional relationships
are present. After some googling, I've used a solution that, although certainly not the best (OpenSessionInViewFilter), 
did the trick. In a real project, with more time, I would be able to pick a better solution. I always look for the appropriate
solution, not the first one.

#### Lack of validation
Due to time constraint, I have not been able to build a proper validation routine (except for the presence of Id when updating or deleting).
I could have created a custom request object and worked with Jackson coupled with some Hibernate validations (@NotNull etc.).

#### Lack of unit tests
Again due to time constraint, I have not been able to code any unit tests. However, I have basic knowledge and command of
common test frameworks like Junit and Mockito.

#### Better response
I have tried to delivery appropriated responses to every call made to this API, at least regarding HTTP status. However, my responses
could have been more polished if I have more time to study the JAX-RS API.
package com.productapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.productapi.model.Image;
import com.productapi.repository.ImageRepository;

@Service
public class ImageService {

	@Autowired
	private ImageRepository repo;
	
	@Autowired
	private ProductService productService;
	
	@Transactional(readOnly=true)
	public List<Image> getImages() {
		return repo.findAll();
	}
	
	public Image findById(Long id) {
		try {
			return repo.findOne(id);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}
	
	public Image save(Image image) {
		try {
			if(image.getProduct() != null && image.getProduct().getId() != null && 
				productService.findById(image.getProduct().getId()) == null) {
				return null;
			}
			
			Image i = repo.save(image);
			return i;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public void delete(Long id) {
		repo.delete(id);
	}
	
	public Image update(Image image) {
		return repo.save(image);
	}
	
	public List<Image> getImageSetByProductId(Long id) {
		return repo.findByProductId(id);
	}
}

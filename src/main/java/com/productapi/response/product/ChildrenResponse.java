package com.productapi.response.product;

import java.util.List;

import com.productapi.model.Product;

public class ChildrenResponse {

	public ChildrenResponse() {}
	
	public ChildrenResponse(List<Product> children) {
		this.children = children;
	}
	
	private List<Product> children;

	public List<Product> getChildren() {
		return children;
	}

	public void setChildren(List<Product> children) {
		this.children = children;
	}
}

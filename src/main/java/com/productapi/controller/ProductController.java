package com.productapi.controller;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.productapi.model.Product;
import com.productapi.response.product.ChildrenResponse;
import com.productapi.response.product.ComplexProductResponse;
import com.productapi.response.product.SimpleProductResponse;
import com.productapi.service.ProductService;

@Component
@Path("/product")
public class ProductController {

	private static final String URL = "/app/product/"; 
	
	@Autowired
	private ProductService service;
	
	@GET
	@Path("/getAllComplexProducts")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllComplexProducts() {
		List<Product> products = service.getProducts();
		return Response.ok(ComplexProductResponse.fromListProduct(products)).build();
	}
	
	@GET
	@Path("/getAllSimpleProducts")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllSimpleProducts() {
		List<Product> products = service.getProducts();
		return Response.ok(SimpleProductResponse.fromListProduct(products)).build();
	}
	
	@GET
	@Path("/getChildren/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getChildreb(@PathParam("id") Long id) {
		List<Product> children = service.getChildren(id);
		return Response.ok(new ChildrenResponse(children)).build();
	}
	
	@GET
	@Path("/complex/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getComplexProductById(@PathParam("id") Long id) {
		Product product = service.findById(id);
		return Response.ok(ComplexProductResponse.fromProduct(product)).build();
	}
	
	@GET
	@Path("/simple/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSimpleProductById(@PathParam("id") Long id) {
		Product product = service.findById(id);
		return Response.ok(SimpleProductResponse.fromProduct(product)).build();
	}
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addProduct(Product product) {
		Product p = service.save(product);
		
		if(p == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		return Response.created(URI.create(URL + p.getId())).build();
		
	}
	
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProduct(Product product) {
		if(product.getId() == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		Product retrievedProduct = service.findById(product.getId());
		if(retrievedProduct == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		service.save(product);
		return Response.accepted().build();
	}
	
	@POST
	@Path("/delete/{id}")
	public Response deleteProduct(@PathParam("id")Long id) {
		service.delete(id);
		return Response.ok().build();
	}
}

package com.productapi.response.image;

import java.util.ArrayList;
import java.util.List;

import com.productapi.model.Image;

public class ImageResponse {

	private Long id;
	
	private String type;
	
	public ImageResponse(){}
	
	public ImageResponse(Image image) {
		id = image.getId();
		type = image.getType();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static List<ImageResponse> getImages(List<Image> imagesList) {
		List<ImageResponse> list = new ArrayList<>();
		for (Image image : imagesList) {
			list.add(new ImageResponse(image));
		}
		
		return list;
	}
}

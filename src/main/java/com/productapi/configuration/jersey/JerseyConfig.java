package com.productapi.configuration.jersey;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.productapi.controller.ImageController;
import com.productapi.controller.ProductController;

@Component
@ApplicationPath("/app")
public class JerseyConfig extends ResourceConfig {
	public JerseyConfig() {
		register(ProductController.class);
		register(ImageController.class);
	}
}

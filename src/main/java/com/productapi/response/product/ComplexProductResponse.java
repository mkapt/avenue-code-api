package com.productapi.response.product;

import java.util.ArrayList;
import java.util.List;

import com.productapi.model.Product;

public class ComplexProductResponse extends ProductResponse {

	ComplexProductResponse(){}
	
	ComplexProductResponse(Product product) {
		super.id = product.getId();
		super.name = product.getName();
		super.description = product.getDescription();
		this.children = product.getChildren();
	}
	
	private List<Product> children;
	
	public List<Product> getChildren() {
		return children;
	}

	public void setChildren(List<Product> children) {
		this.children = children;
	}

	public static ComplexProductResponse fromProduct(Product product) {
		return new ComplexProductResponse(product);
	}
	
	public static List<ComplexProductResponse> fromListProduct(List<Product> products) {
		List<ComplexProductResponse> list = new ArrayList<>();
		for (Product product : products) {
			list.add(new ComplexProductResponse(product));
		}
		
		return list;
	}
}

package com.productapi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.productapi.model.Product;
import com.productapi.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository repo;
	
	@Transactional(readOnly=true)
	public List<Product> getProducts() {
		return repo.findAll();
	}
	
	public Product findById(Long id) {
		try {
			return repo.findOne(id);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}
	
	public List<Product> getChildren(Long id) {
		Product p = repo.findOne(id);
		if(p != null) {
			return p.getChildren();
		}
		
		return new ArrayList<Product>();
	}
	
	public Product save(Product product) {
		if(product.getChildren() != null && !product.getChildren().isEmpty()) {
			List<Product> children = new ArrayList<>();
			for (Product child : product.getChildren()) {
				child.setParent(product);
				children.add(child);
			}
			product.setChildren(children);
		}
		
		try {
			Product p = repo.save(product);
			return p;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public void delete(Long id) {
		repo.delete(id);
	}
	
	public Product update(Product product) {
		return repo.save(product);
	}
}

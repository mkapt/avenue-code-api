package com.productapi.configuration.hibernate;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;

@Configuration
public class HibernateConfiguration {

	
	//I've had issues with lazy loading exceptions and, although not considered
	//the best approach by many on the Internet (a.k.a. Stack Overflow site),
	//it's solved my problem for now.
	
    @Bean
    public FilterRegistrationBean registerOpenSessionInViewFilterBean() {
    		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        OpenEntityManagerInViewFilter filter = new OpenEntityManagerInViewFilter();
        registrationBean.setFilter(filter);
        registrationBean.setOrder(5);
        return registrationBean;
    }
}
